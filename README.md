# French Chatbot Base
This repository is a complete starting kit to create a RASA french chatbot

The configuration, domain and data files contain everything necessary to start a RASA chatbot.
It already provides basic intents, rules and nlu data that can be useful for any chatbot (greeting, bye, affirm, deny, etc.).

Also provided in `deployment/` is an example of deployment configuration using docker-compose overrides on top of the default configuration presented in Rasa X docs: https://rasa.com/docs/rasa-x/installation-and-setup/install/docker-compose. See [the corresponding readme](deployment/README.md) for more information.


