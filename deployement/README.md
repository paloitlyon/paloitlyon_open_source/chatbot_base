# Deployement Instructions
## Prerequisites
- A correctly sized VM (see https://rasa.com/docs/rasa-x/installation-and-setup/install/docker-compose#requirements)
- Rasa X docker compose installation complete (https://rasa.com/docs/rasa-x/installation-and-setup/install/docker-compose)

## Customization
### Description of docker images
In this repository, you will find 2 Dockerfiles:
- At the root of the repo, the Dockerfile is the image for the RASA NLU engine, it will run the Rasa NLU components, hence the full pipeline given in the `config.yml` file. This Dockerfile doesn't need to be reuilt often, except if your nlu pipeline changes and requires other libraries or runs some custom code (e.g. a custom Spacy pipeline for NER, relation extraction etc.)
- In the `actions/` folder the Dockerfile is for the image of the Rasa Action Server. This image will run the code contained in the actions folder and must be rebuilt after each update of the custom actions.
### Upload docker images to your personal docker registry
To be able to use your own customized images of Rasa NLU or Rasa Action Server, you'll need to upload the images to a docker hub.

## Deployment: at initialization
Once connected on your VM, copy the content the `docker-compose.overrides.yml` file into your rasa X deployment config folder (default: `/etc/rasa/docker-compose.overrides.yml` file needs to be created).
Don't forget to customize the image name and version to match you custom image. I've left my image names as examples, they can be used directly for basic french chatbots, but you'll have to customize at least the action server image in order to be able to run you own custom actions.

